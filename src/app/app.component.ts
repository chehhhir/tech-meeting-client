import {Component, OnInit} from '@angular/core';
import {LoremIpsum} from "lorem-ipsum";
import {defer, delay, EMPTY, Observable, of} from "rxjs";
import {environment} from "../environment";
import {HttpClient} from "@angular/common/http";

export interface Post {
  title: string;
  content: string;
  createDate: Date;
}

export interface Page<T> {
  content: T[];
  pageable: {
    pageNumber: number;
    pageSize: number;
    sort?: {
      sorted: boolean;
      unsorted: boolean;
    };
  };
  totalPages: number;
  totalElements: number;
  last: boolean;
  size: number;
  number: number;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  protected readonly environment = environment;
  title = environment.appName;
  posts: Observable<Page<Post>> = EMPTY;


  constructor(private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    this.posts = this.httpClient.get<Page<Post>>(environment.api + "/posts");
  }

  randomDate(start: Date, end: Date) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }


}
