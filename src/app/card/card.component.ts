import { Component, Input } from '@angular/core';
import { Post } from "../app.component";
import { environment } from "../../environment";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() post!: Post;
  topics = environment.topics;
  randomTopics: string[];

  constructor() {
    const minRange = 1;
    const maxRange = Math.min(4, this.topics.length);

    const randomTopicsCount = Math.floor(Math.random() * (maxRange - minRange + 1)) + minRange;
    const randomIndexes = new Set<number>();

    while (randomIndexes.size < randomTopicsCount) {
      const randomIndex = Math.floor(Math.random() * maxRange);
      randomIndexes.add(randomIndex);
    }

    this.randomTopics = Array.from(randomIndexes).map(index => this.topics[index]);

  }
}
